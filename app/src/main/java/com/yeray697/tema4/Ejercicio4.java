package com.yeray697.tema4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Ejercicio4 extends AppCompatActivity {

    ListView lvEnfermedades;
    private static final String URL = "http://yeray697.esy.es/images/spain_death.json";
    ArrayList<Enfermedad> muertes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);
        if (Utilities.isNetworkAvailable(this)){
            RestClient.get(URL, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String json = new String(responseBody);
                    try {
                        muertes = Utilities.analizarMuertes(new JSONObject(json));
                        lvEnfermedades.setAdapter(new ArrayAdapter<>(Ejercicio4.this,android.R.layout.simple_list_item_1,muertes));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(Ejercicio4.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        } else {
            Toast.makeText(this, "No hay internet", Toast.LENGTH_SHORT).show();
            finish();
        }
        lvEnfermedades = (ListView) findViewById(R.id.lv4);
        lvEnfermedades.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Ejercicio4.this, Ejercicio4_2.class);
                intent.putExtra("enfermedad",muertes.get(position));
                startActivity(intent);
            }
        });
    }
}
