package com.yeray697.tema4;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by yeray697 on 8/01/17.
 */

public class CiudadAdapter extends ArrayAdapter<Tiempo> {
    public CiudadAdapter(Context context, List<Tiempo> objects) {
        super(context, R.layout.item_ciudad_list, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.item_ciudad_list,parent,false);
            holder = new Holder(view);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        Tiempo ciudad = getItem(position);

        holder.tvDia.setText(new SimpleDateFormat("dd/MM/yyyy").format(new Date(Long.parseLong(ciudad.getDia()) * 1000)));
        holder.tvTempMin.setText(String.format("%.2fºC", Double.parseDouble(ciudad.getTemperature().getMinTemperature()) - 273.15));
        holder.tvTempMax.setText(String.format("%.2fºC", Double.parseDouble(ciudad.getTemperature().getMaxTemperature()) - 273.15));
        holder.tvViento.setText(ciudad.getViento() + " m/s");
        holder.tvHumedadPresion.setText(ciudad.getWeather()[0].getCielo()+": "+ciudad.getHumedad() + "%, " +ciudad.getPresion() + " hpa");
        Picasso.with(getContext())
                .load("http://openweathermap.org/img/w/" + ciudad.getWeather()[0].getImage() + ".png")
                .into(holder.ivCielo);
        return view;
    }

    class Holder{
        TextView tvDia, tvTempMax, tvTempMin, tvViento, tvHumedadPresion;
        ImageView ivCielo;
        public Holder(View view){
            tvDia = (TextView) view.findViewById(R.id.tvDia_item);
            tvTempMax = (TextView) view.findViewById(R.id.tvMaxTemp_item);
            tvTempMin = (TextView) view.findViewById(R.id.tvMinTemp_item);
            tvViento = (TextView) view.findViewById(R.id.tvViento_item);
            tvHumedadPresion = (TextView) view.findViewById(R.id.tvHumedadPresion_item);
            ivCielo = (ImageView) view.findViewById(R.id.ivCielo_item);
        }
    }
}
