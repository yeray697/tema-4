package com.yeray697.tema4;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by yeray697 on 9/01/17.
 */

public class Enfermedad implements Parcelable {
    private String nombre;
    private String year;
    private ArrayList<MuertesEdad> muertes;

    public Enfermedad(){

    }

    protected Enfermedad(Parcel in) {
        nombre = in.readString();
        year = in.readString();
        muertes = in.createTypedArrayList(MuertesEdad.CREATOR);
    }

    public static final Creator<Enfermedad> CREATOR = new Creator<Enfermedad>() {
        @Override
        public Enfermedad createFromParcel(Parcel in) {
            return new Enfermedad(in);
        }

        @Override
        public Enfermedad[] newArray(int size) {
            return new Enfermedad[size];
        }
    };

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public ArrayList<MuertesEdad> getMuertes() {
        return muertes;
    }

    public void setMuertes(ArrayList<MuertesEdad> muertes) {
        this.muertes = muertes;
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(year);
        dest.writeTypedList(muertes);
    }
}
