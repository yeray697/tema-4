package com.yeray697.tema4;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Xml;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by yeray697 on 8/01/17.
 */

public class Utilities {
    private static float ratio;

    public static double dollarToEuro(double dollars) {
        return dollars / ratio;
    }

    public static double euroToDollar(double euros) {
        return euros * ratio;
    }

    public static void analizarMonedas(JSONObject response) throws JSONException {
        JSONObject monedas;
        monedas = (JSONObject) response.get("rates");
        Iterator<String> iter = monedas.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                ratio = Float.parseFloat(monedas.get(key).toString());
            } catch (Exception e) {
                e.getMessage();
            }


        }
    }

    public static Ciudad analizarCiudad(JSONObject jsonObject) {
        Ciudad city = null;
        String ciudad = "-", temperatura = "-", viento = "-", cielo = "-", presion = "-", presionMar = "-", humedad = "-", amanecer = "-", atardecer = "-";
        try {
            JSONObject main = jsonObject.getJSONObject("main");
            JSONArray weather = jsonObject.getJSONArray("weather");
            JSONObject wind = jsonObject.getJSONObject("wind");
            JSONObject sys = jsonObject.getJSONObject("sys");

            try {
                ciudad = jsonObject.get("name").toString();
            } catch (JSONException e) {
            }
            try {
                temperatura = main.get("temp").toString();
            } catch (JSONException e) {
            }
            try {
                viento = wind.get("speed").toString();
            } catch (JSONException e) {
            }
            try {
                cielo = ((JSONObject) weather.get(0)).get("description").toString();
            } catch (JSONException e) {
            }
            try {
                presion = main.get("pressure").toString();
            } catch (JSONException e) {
            }
            try {
                presionMar = main.get("sea_level").toString();
            } catch (JSONException e) {
            }
            try {
                humedad = main.get("humidity").toString();
            } catch (JSONException e) {
            }
            try {
                amanecer = sys.get("sunrise").toString();
            } catch (JSONException e) {
            }
            try {
                atardecer = sys.get("sunset").toString();
            } catch (JSONException e) {
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new Ciudad(ciudad, temperatura, viento, cielo, presion, presionMar, humedad, amanecer, atardecer);
    }

    public static ArrayList<Tiempo> analizarTiempo(String json) throws JSONException {
        JSONArray array = new JSONObject(json).getJSONArray("list");
        ArrayList<Tiempo> tiempo;
        Type type = new TypeToken<ArrayList<Tiempo>>() {
        }.getType();
        Gson gson = new Gson();
        tiempo = gson.fromJson(String.valueOf(array), type);

        try {
            saveToJSON(tiempo);
            saveToXML(tiempo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tiempo;
    }

    private static void saveToJSON(ArrayList<Tiempo> tiempo) throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonString = gson.toJson(tiempo);
        FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "tiempo.json"));
        fos.write(jsonString.getBytes());
        fos.close();
    }

    private static void saveToXML(ArrayList<Tiempo> tiempo) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "tiempo.xml"));
        XmlSerializer xml = Xml.newSerializer();
        xml.setOutput(fos, "UTF-8");
        xml.startDocument(null, true);
        xml.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        xml.startTag(null, "tiempo");
        for (int i = 0; i < tiempo.size(); i++) {
            xml.startTag(null, "tiempo_diario");

            createEntry(xml, "dia", tiempo.get(i).getDia());
            createEntry(xml, "icono", tiempo.get(i).getWeather()[0].getImage());
            createEntry(xml, "temp_max", tiempo.get(i).getTemperature().getMaxTemperature());
            createEntry(xml, "temp_min", tiempo.get(i).getTemperature().getMinTemperature());
            createEntry(xml, "humedad", tiempo.get(i).getHumedad());
            createEntry(xml, "presion", tiempo.get(i).getPresion());
            createEntry(xml, "viento", tiempo.get(i).getViento());
            createEntry(xml, "cielo", tiempo.get(i).getWeather()[0].getCielo());

            xml.endTag(null, "tiempo_diario");
        }
        xml.endTag(null, "tiempo");
        xml.endDocument();
        xml.flush();
        fos.close();
    }

    private static void createEntry(XmlSerializer xml, String tag, String value) throws IOException {
        xml.startTag(null, tag);
        xml.text(value);
        xml.endTag(null, tag);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public static ArrayList<Enfermedad> analizarMuertes(JSONObject jsonObject) throws JSONException {
        JSONArray array = jsonObject.getJSONArray("fact");
        JSONObject aux;
        ArrayList<Muertes> muertesArray = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            aux = (JSONObject) array.get(i);
            if (aux.getJSONObject("dims").getString("YEAR").equals("2015")) {
                Muertes muertes = new Muertes();
                muertes.setYear("2015");
                muertes.setEnfermedad(aux.getJSONObject("dims").getString("CHILDCAUSE"));
                muertes.setEdad(aux.getJSONObject("dims").getString("AGEGROUP"));
                muertes.setMuertos(aux.getString("Value"));
                muertesArray.add(muertes);
            }
        }
        String enfermedadAnterior = "";
        ArrayList<Enfermedad> enfermedades = new ArrayList<>();
        for (Muertes muerte: muertesArray) {
            if (muerte.getEnfermedad().equals(enfermedadAnterior) && !enfermedadAnterior.equals("")) {
                MuertesEdad muertesEdad = new MuertesEdad();
                muertesEdad.setEdad(muerte.getEdad());
                muertesEdad.setMuertes(muerte.getMuertos());
                enfermedades.get(enfermedades.size() - 1).getMuertes().add(muertesEdad);
            } else {
                enfermedadAnterior = muerte.getEnfermedad();
                Enfermedad enfermedad = new Enfermedad();
                enfermedad.setYear(muerte.getYear());
                enfermedad.setNombre(muerte.getEnfermedad());
                ArrayList<MuertesEdad> muertesEdad = new ArrayList<>();
                MuertesEdad muertesEdad1 = new MuertesEdad();
                muertesEdad1.setEdad(muerte.getEdad());
                muertesEdad1.setMuertes(muerte.getMuertos());
                muertesEdad.add(muertesEdad1);
                enfermedad.setMuertes(muertesEdad);
                enfermedades.add(enfermedad);
            }
        }
        return enfermedades;
    }

}
