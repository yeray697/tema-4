###### En todos los ejercicios se controla la rotación de pantalla y que haya conexión a internet.
###### Para la conexión a internet se ha utilizado la clase RestClient.
###### .
#### 1. Crear  una aplicación que muestre una lista de ciudades (málaga, madrid, barcelona, . . .  ) y, cuando se elija una, se mostrará en una nueva ventana información del tiempo (temperatura, presión y humedad) en esa ciudad. Los datos meteorológicos se obtendrán en formato JSON de Open Weather Map.
###### Hace lo que se pide.
###### .
#### 2. Crear una aplicación que pida una ciudad y guarde su previsión meteorológica para los próximos 7 días facilitada en formato JSON por Open Weather Map. Para cada día se almacenarán la fecha, las temperaturas mínima y máxima y la presión. Se crearán dos ficheros en la tarjeta de memoria: uno en formato XML y otro en formato JSON. Se deberá usar la biblioteca Gson.
###### Hace lo que se pide.
###### Crea el XML y el JSON con un formato diferente al descargado, guardando solo los datos que utiliza la aplicación.
###### .
#### 3. Buscar alguna web en Internet que facilite el valor actual del cambio de euro a dólar en formato XML o JSON.Los datos deben ser fiables. Modificar la aplicación Conversor de moneda para que descargue dicho valor y lo utilice para realizar una conversión.
###### Hace lo que se pide.
###### Fuente del JSON: http://api.fixer.io/latest?symbols=USD.
###### .
#### 4. Buscar alguna web en Internet que facilite información interesante en XML o JSON. También se puede crear un documento propio en formato XML o JSON y almacenarlo en el servidor web local o en un alojamiento propio en Internet, en lugar de acceder a la información facilitada por un sitio web. Crear una aplicación que descargue esa información y realice alguna tarea con ella. La nueva aplicación será diferente a las vistas en clase (obtener las noticias de un sitio web, mostrar el tiempo y temperaturas, cambiar divisas, etc ). Se valorará la utilización de datos abiertos en Málaga, datos abiertos del Gobierno de España o fuentes de datos públicos y abiertos, como por ejempo MadBike.
###### Descarga un fichero JSON con las tasas de mortalidad infantil en España, y muestra solo las de 2015. Se puede elegir en una lista qué enfermedad mirar.
###### Fuente del JSON: http://apps.who.int/gho/data/view.main.ghe1002015-ESP?lang=en