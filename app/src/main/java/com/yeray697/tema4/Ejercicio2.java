package com.yeray697.tema4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class Ejercicio2 extends AppCompatActivity {

    private static final String WEB = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
    private static final String API_KEY ="&APPID=9fc5e25b05a639bec9cbce6a2381e899";

    ListView lvTiempo;
    EditText etSearch;
    ImageView ivSearch;
    CiudadAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);
        lvTiempo = (ListView) findViewById(R.id.lv2);
        etSearch = (EditText) findViewById(R.id.etSearch);
        ivSearch = (ImageView) findViewById(R.id.ivSearch);
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utilities.isNetworkAvailable(Ejercicio2.this)) {
                    if (!TextUtils.isEmpty(etSearch.getText())) {
                        String url = WEB + etSearch.getText() + API_KEY;
                        RestClient.get(url, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                String json = new String(responseBody);
                                try {
                                    adapter = new CiudadAdapter(Ejercicio2.this, Utilities.analizarTiempo(json));
                                    lvTiempo.setAdapter(adapter);
                                } catch (JSONException e) {
                                    Toast.makeText(Ejercicio2.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    if (adapter != null)
                                        adapter.clear();
                                }

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Toast.makeText(Ejercicio2.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                if (adapter != null)
                                    adapter.clear();
                            }
                        });
                    }
                } else {
                    Toast.makeText(Ejercicio2.this, "No tienes internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
