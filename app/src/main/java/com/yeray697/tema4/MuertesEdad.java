package com.yeray697.tema4;

import android.os.Parcel;
import android.os.Parcelable;

public class MuertesEdad implements Parcelable {
    private String edad;
    private String muertes;

    public MuertesEdad(){

    }

    protected MuertesEdad(Parcel in) {
        edad = in.readString();
        muertes = in.readString();
    }

    public static final Creator<MuertesEdad> CREATOR = new Creator<MuertesEdad>() {
        @Override
        public MuertesEdad createFromParcel(Parcel in) {
            return new MuertesEdad(in);
        }

        @Override
        public MuertesEdad[] newArray(int size) {
            return new MuertesEdad[size];
        }
    };

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getMuertes() {
        return muertes;
    }

    public void setMuertes(String muertes) {
        this.muertes = muertes;
    }

    @Override
    public String toString() {
        return "Edad: " + edad + ", muertes: " + muertes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(edad);
        dest.writeString(muertes);
    }
}