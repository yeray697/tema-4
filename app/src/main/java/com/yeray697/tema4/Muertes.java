package com.yeray697.tema4;

import java.util.ArrayList;

/**
 * Created by yeray697 on 9/01/17.
 */

public class Muertes{
    private String enfermedad;
    private String year;
    private String edad;
    private String muertos;

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getMuertos() {
        return muertos;
    }

    public void setMuertos(String muertos) {
        this.muertos = muertos;
    }
}