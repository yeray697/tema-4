package com.yeray697.tema4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Ejercicio1 extends AppCompatActivity {

    public static final String CIUDAD_KEY = "ciudad";
    ListView lvCiudades;
    ArrayList<String> ciudades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);
        ciudades = new ArrayList<>();
        ciudades.add("Málaga");
        ciudades.add("Madrid");
        ciudades.add("Barcelona");
        ciudades.add("Valencia");
        ciudades.add("Pontevedra");
        ciudades.add("Huelva");
        ciudades.add("Jaén");
        ciudades.add("Ibiza");
        ciudades.add("Murcia");
        ciudades.add("Castellón");
        ciudades.add("Alicante");
        ciudades.add("Bilbao");
        lvCiudades = (ListView) findViewById(R.id.lv1);
        ListAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,ciudades);
        lvCiudades.setAdapter(adapter);
        lvCiudades.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (Utilities.isNetworkAvailable(Ejercicio1.this)) {
                    Intent intent = new Intent(Ejercicio1.this, Ejercicio1_2.class);
                    intent.putExtra(CIUDAD_KEY, parent.getItemAtPosition(position).toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(Ejercicio1.this, "No tienes internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
