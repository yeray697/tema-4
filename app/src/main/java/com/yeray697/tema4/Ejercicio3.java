package com.yeray697.tema4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class Ejercicio3 extends AppCompatActivity implements View.OnClickListener {

    private static final String NONUMBERERROR = "Introduce un número";

    private static final String URL_RATIO = "http://api.fixer.io/latest?symbols=USD";

    private EditText etDollar, etEuro;
    private RadioButton rbDtoE, rbEtoD;
    private Button btConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);
        setTitle("Conversor");
        etDollar = (EditText) findViewById(R.id.etDollar1);
        etEuro = (EditText) findViewById(R.id.etEuro1);
        rbDtoE = (RadioButton) findViewById(R.id.rbDtoE1);
        rbEtoD = (RadioButton) findViewById(R.id.rbEtoD1);
        btConvert = (Button) findViewById(R.id.btConvert1);
        rbDtoE.setOnClickListener(this);
        rbEtoD.setOnClickListener(this);
        btConvert.setOnClickListener(this);
        etDollar.requestFocus();
        etEuro.setEnabled(false);
        getRatio();
    }

    private void getRatio() {
        btConvert.setEnabled(false);
        if (Utilities.isNetworkAvailable(Ejercicio3.this)) {
            RestClient.get(URL_RATIO, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String json = new String(responseBody);
                    try {
                        Utilities.analizarMonedas(new JSONObject(json));
                    } catch (JSONException e) {
                        Toast.makeText(Ejercicio3.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    btConvert.setEnabled(true);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(Ejercicio3.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    btConvert.setEnabled(true);
                }
            });
        } else {
            Toast.makeText(this, "No tienes internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btConvert1:
                convert();
                break;
            case R.id.rbDtoE1:
                radioButtonFocus(true);
                break;
            case R.id.rbEtoD1:
                radioButtonFocus(false);
                break;
            default:
                break;
        }
    }

    private void convert() {
        try {
            double currency;
            if (rbDtoE.isChecked()) { //Dollars to euros
                currency = Double.parseDouble(etDollar.getText().toString());
                double result = Utilities.dollarToEuro(currency);
                etEuro.setText(String.format(Locale.US, "%.4f", result));
            } else {
                currency = Double.parseDouble(etEuro.getText().toString());
                double result = Utilities.euroToDollar(currency);
                etDollar.setText(String.format(Locale.US, "%.4f", result));
            }
        }
        catch (Exception ex) {
            Toast.makeText(this, NONUMBERERROR, Toast.LENGTH_SHORT).show();
        }
    }


    private void radioButtonFocus(boolean checked) {
        etDollar.setEnabled(checked);
        etEuro.setEnabled(!checked);
        if (checked)
            etDollar.requestFocus();
        else
            etEuro.requestFocus();
    }
}
