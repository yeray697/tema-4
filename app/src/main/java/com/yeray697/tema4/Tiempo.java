package com.yeray697.tema4;

import com.google.gson.annotations.SerializedName;

class Tiempo {
    @SerializedName("dt")
    private String dia;
    @SerializedName("temp")
    private Temperature temperature;
    @SerializedName("pressure")
    private String presion;
    @SerializedName("speed")
    private String viento;
    @SerializedName("weather")
    private Weather[] weather;
    @SerializedName("humidity")
    private String humedad;

    public Tiempo(String dia, Temperature temperature,Weather[] weather, String presion, String viento, String humedad) {
        this.dia = dia;
        this.temperature = temperature;
        this.weather = weather;
        this.presion = presion;
        this.viento = viento;
        this.humedad = humedad;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }


    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public String getPresion() {
        return presion;
    }

    public void setPresion(String presion) {
        this.presion = presion;
    }

    public String getViento() {
        return viento;
    }

    public void setViento(String viento) {
        this.viento = viento;
    }

    public String getHumedad() {
        return humedad;
    }

    public void setHumedad(String humedad) {
        this.humedad = humedad;
    }

    class Temperature {
        @SerializedName("max")
        private String maxTemperature;
        @SerializedName("min")
        private String minTemperature;

        public String getMaxTemperature() {
            return maxTemperature;
        }

        public void setMaxTemperature(String maxTemperature) {
            this.maxTemperature = maxTemperature;
        }

        public String getMinTemperature() {
            return minTemperature;
        }

        public void setMinTemperature(String minTemperature) {
            this.minTemperature = minTemperature;
        }
    }
    class Weather{
        @SerializedName("description")
        private String cielo;
        @SerializedName("icon")
        private String image;

        public String getCielo() {
            return cielo;
        }

        public void setCielo(String cielo) {
            this.cielo = cielo;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
