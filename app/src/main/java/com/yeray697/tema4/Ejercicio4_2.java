package com.yeray697.tema4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Ejercicio4_2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4_2);
        Enfermedad enfermedad = getIntent().getExtras().getParcelable("enfermedad");
        ListView lvEnfermedad = (ListView) findViewById(R.id.lv4_2);
        lvEnfermedad.setAdapter(new ArrayAdapter<>(Ejercicio4_2.this,android.R.layout.simple_list_item_1,enfermedad.getMuertes()));
    }
}
