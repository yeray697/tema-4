package com.yeray697.tema4;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class Ejercicio1_2 extends AppCompatActivity {

    private static final String WEB = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static final String API_KEY ="&APPID=9fc5e25b05a639bec9cbce6a2381e899";

    Ciudad ciudad;

    String ciudad_name;

    TextView tvCiudad, tvTemperatura, tvViento, tvCielo, tvPresion, tvPresionMar, tvHumedad, tvAmanecer, tvAtardecer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1_2);
        ciudad_name = getIntent().getExtras().getString(Ejercicio1.CIUDAD_KEY);
        tvCiudad = (TextView) findViewById(R.id.tvCiudad1);
        tvTemperatura = (TextView) findViewById(R.id.tvTemperatura1);
        tvViento = (TextView) findViewById(R.id.tvViento1);
        tvCielo = (TextView) findViewById(R.id.tvCielo1);
        tvPresion = (TextView) findViewById(R.id.tvPresion1);
        tvPresionMar = (TextView) findViewById(R.id.tvPresionMar1);
        tvHumedad = (TextView) findViewById(R.id.tvHumedad1);
        tvAmanecer = (TextView) findViewById(R.id.tvAmanecer1);
        tvAtardecer = (TextView) findViewById(R.id.tvAtardecer1);
        getCiudad();

    }

    private void getCiudad(){
        String url = WEB+ciudad_name+API_KEY;
        RestClient.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String json = new String(responseBody);
                try {
                    ciudad = Utilities.analizarCiudad(new JSONObject(json));
                    tvCiudad.setText(ciudad.getCiudad());

                    tvTemperatura.setText(setBoldText("Temperatura",String.format("%.2fºC", Double.parseDouble(ciudad.getTemperatura()) - 273.15)));
                    tvViento.setText(setBoldText("Viento",ciudad.getViento() + " m/s"));
                    tvCielo.setText(setBoldText("Cielo",ciudad.getCielo()));
                    tvPresion.setText(setBoldText("Presión",ciudad.getPresion() + " hpa"));
                    tvPresionMar.setText(setBoldText("Presión (nivel del mar)",ciudad.getPresionMar()+ " hpa"));
                    tvHumedad.setText(setBoldText("Humedad",ciudad.getHumedad() + "%"));

                    long amanecerLong = Long.valueOf(ciudad.getAmanecer())*1000;
                    long atardecerLong = Long.valueOf(ciudad.getAtardecer())*1000;
                    Date amanecerDate = new java.util.Date(amanecerLong);
                    Date atardecerDate = new java.util.Date(atardecerLong);
                    String amanecer = new SimpleDateFormat("hh:mm a").format(amanecerDate);
                    String atardecer = new SimpleDateFormat("hh:mm a").format(atardecerDate);

                    tvAmanecer.setText(setBoldText("Amanecer",amanecer));
                    tvAtardecer.setText(setBoldText("Atardecer",atardecer));
                } catch (JSONException e) {
                    Toast.makeText(Ejercicio1_2.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(Ejercicio1_2.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private Spanned setBoldText(String title, String subtitle) {
        String text = "<b>"+title+"</b> "+subtitle;
        Spanned spannedText;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            spannedText = Html.fromHtml(text,Html.FROM_HTML_MODE_COMPACT);
        } else {
            spannedText = Html.fromHtml(text);
        }
        return spannedText;
    }
}
