package com.yeray697.tema4;

class Ciudad {
    private String ciudad,
            temperatura,
            viento,
            cielo,
            presion,
            presionMar,
            humedad,
            amanecer,
            atardecer;

    public Ciudad(String ciudad, String temperatura, String viento, String cielo, String presion, String presionMar, String humedad, String amanecer, String atardecer) {
        this.ciudad = ciudad;
        this.temperatura = temperatura;
        this.viento = viento;
        this.cielo = cielo;
        this.presion = presion;
        this.presionMar = presionMar;
        this.humedad = humedad;
        this.amanecer = amanecer;
        this.atardecer = atardecer;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public String getViento() {
        return viento;
    }

    public String getCielo() {
        return cielo;
    }

    public String getPresion() {
        return presion;
    }

    public String getPresionMar() {
        return presionMar;
    }

    public String getHumedad() {
        return humedad;
    }

    public String getAmanecer() {
        return amanecer;
    }

    public String getAtardecer() {
        return atardecer;
    }
}
